import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { IsibahayaPage } from '../isibahaya/isibahaya';
import { LihatTempatBerbahayaPage } from '../LihatTempatBerbahaya/LihatTempatBerbahaya';
import { Geolocation } from '@ionic-native/geolocation';
import firebase from 'firebase';
import { ModalController } from 'ionic-angular';

declare var google;

@Component({
  selector: 'home-page',
  templateUrl: 'home.html'
})
export class HomePage {
 public items: Array<any> = [];
 public itemRef: firebase.database.Reference = firebase.database().ref('/reports'); 
 

 @ViewChild('map') mapElement: ElementRef;
  map: any;
  constructor(public navCtrl: NavController, public geolocation: Geolocation, public modalCtrl: ModalController) {
  }

  ionViewDidLoad(){
	this.itemRef.on('value', itemSnapshot => {
    this.items = [];
    itemSnapshot.forEach( itemSnap => {
      this.items.push(itemSnap.val());
      return false;
    });
  });
    this.loadMap();
  }

  loadMap(){
    this.geolocation.getCurrentPosition().then((position) => {
    let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
 
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        fullscreenControl: false,
        streetViewControl: false
      }
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let content = this.modalCtrl.create(LihatTempatBerbahayaPage);
      this.addInfoWindow(this.addPositionMarker(latLng,this.map),content)
      this.retrieveLaporan();
      }, (err) => {
      console.log(err);
    });
  }

  addPositionMarker(position,map){
    return new google.maps.Marker({
      position,
      map
    })
  }

  addInfoWindow(marker, content){
  let infoWindow = new google.maps.InfoWindow({
    content: content
  });
 }

 addEventClick(marker, key){
  google.maps.event.addListener(marker, 'click', () => {
    this.navCtrl.push(LihatTempatBerbahayaPage, {
      param1: key
    })
  }); 
 }
  
  lapor(){
	this.navCtrl.push(IsibahayaPage)
  }
  

  retrieveLaporan(){
    const reportRef: firebase.database.Reference= firebase.database().ref('/reports/')
    var laporan;
    var keys;
    reportRef.on('value', laporanSnapshot => {	
      laporan = laporanSnapshot.val();
  	  keys = Object.keys(laporan);
  	  for (var i=0; i<keys.length ; i++) {
        var k = keys[i];
        var lat = laporan[k].latitude + "";
        var long = laporan[k].longitude+ "";
        this.addEventClick(this.addMarker(lat,long,this.map),k)
      }
    });
  }

  addMarker(lat, long, map, key){
    let position = new google.maps.LatLng(lat,long);
    return new google.maps.Marker({
      position,
      map
    })
  }
}
