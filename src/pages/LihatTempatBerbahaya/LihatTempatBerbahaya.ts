import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';

@Component({
  selector: 'page-LihatTempatBerbahaya',
  templateUrl: 'LihatTempatBerbahaya.html'
})
export class LihatTempatBerbahayaPage {
  public myPerson = {};
  public poto: any;
  public parameter1 = "";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
	this.poto = firebase.database().ref('reports');
	this.parameter1 = navParams.get('param1'); 
  }
  
  ionViewDidLoad() {
  const personRef: firebase.database.Reference = firebase.database().ref(`/reports/` + this.parameter1 + `/`);
  personRef.on('value', personSnapshot => {
	this.myPerson = personSnapshot.val();
  });
  this.getImage(this.myPerson.image)
  }

  getImage(imageName){
  var urlGet = "images/" + imageName;
  var imageURL ="";
  var storageRef = firebase.storage().ref(urlGet);
  storageRef.getDownloadURL().then(url => {
    var src = document.getElementById("image");
    var img = document.createElement("img");
    img.src = url;
    src.appendChild(img);
  }
  );
  console.log(imageURL + "aa")
  }  
}
