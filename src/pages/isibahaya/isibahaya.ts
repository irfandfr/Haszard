import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import firebase from 'firebase';
import { HomePage } from '../home/home';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the IsibahayaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-isibahaya',
  templateUrl: 'isibahaya.html',
})
export class IsibahayaPage {
  base64Image: string;
  alertCtrl: AlertController;

  constructor(public navCtrl: NavController, alertCtrl: AlertController, public navParams: NavParams, private camera: Camera, public geolocation: Geolocation) {
    this.alertCtrl = alertCtrl;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IsibahayaPage');
  }

  createReport(location: string, description: string, latitude : number, longitude : number): void {
  this.geolocation.getCurrentPosition().then((position) => {
      const reportRef: firebase.database.Reference= firebase.database().ref('/reports/')
      console.log("dut "+location)
      latitude = -6.377848
      longitude = 106.831588
      reportRef.push({
        location,
        description,
        latitude,
        longitude
      })
      this.navCtrl.pop()
    });
  }

  takePicture()
  {
    const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
    }
	
	this.camera.getPicture(options).then((imageData) => {
	 // imageData is either a base64 encoded string or a file URI
	 // If it's base64:
	  this.base64Image = 'data:image/jpeg;base64,' + imageData;
	}, (err) => {
	 // Handle error
	});
  }

  upload() {
    let storageRef = firebase.storage().ref();
    // Create a timestamp as filename
    const filename = Math.floor(Date.now() / 1000);

    // Create a reference to 'images/todays-date.jpg'
    const imageRef = storageRef.child(`images/${filename}.jpg`);

    imageRef.putString(this.base64Image, firebase.storage.StringFormat.DATA_URL).then((snapshot)=> {
     // Do something here when the data is succesfully uploaded!
           this.showSuccesfulUploadAlert();

    });
  }

  showSuccesfulUploadAlert() {
    let alert = this.alertCtrl.create({
      title: 'Uploaded!',
      subTitle: 'Picture is uploaded to Firebase',
      buttons: ['OK']
    });
    alert.present();
    // clear the previous photo data in the variable
    this.base64Image = "";
  }
}
