import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IsibahayaPage } from './isibahaya';

@NgModule({
  declarations: [
    IsibahayaPage,
  ],
  imports: [
    IonicPageModule.forChild(IsibahayaPage),
  ],
})
export class IsibahayaPageModule {}
